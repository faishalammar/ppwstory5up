from django import forms
from django.db import models
from .models import ApaKabar
from django.forms import ModelForm

class statusForm(forms.ModelForm):

    class Meta:
        model = ApaKabar
        fields = ('status',)
        widgets = {
            'status': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Bagaimana kabar Anda hari ini ?'}),
        }
