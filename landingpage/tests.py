from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .forms import statusForm
from .views import index
from .models import ApaKabar
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

import unittest


# Create your tests here.

class Lab1UnitTest(TestCase):

    def test_home_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_landingpage_contain_hello(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Apa kabar?', html_response)


    def test_post_form_success_and_render_the_result(self):
        test = 'Test_Status'
        response_post = Client().post('/', {'status' : test})
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_form_validation_for_blank_items(self):
        form = statusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_post_success_and_data_exist_in_database(self):
        Client().post('/', {'status': 'Cek'})
        data_count = ApaKabar.objects.all().count()
        self.assertEqual(data_count,1)

    def test_every_pages_contain_navbar_and_in_navbar_contain_profile_and_home(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('class="nav', html_response)
        self.assertIn('Home', html_response)
        self.assertIn('Profile', html_response)
        response = Client().get('/profile')
        html_response = response.content.decode('utf8')
        self.assertIn('class="nav', html_response)
        self.assertIn('Home', html_response)
        self.assertIn('Profile', html_response)

    def test_profile_is_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code,200)

    def test_profile_page_contains_profile_pic(self):
        response = Client().get('/profile')
        html_response = response.content.decode('utf8')
        self.assertNotIn('<img src=''>', html_response)

    def test_profile_page_contains_name(self):
        response = Client().get('/profile')
        html_response = response.content.decode('utf8')
        self.assertNotIn('<h1 id="nama"></h1>', html_response)

    def test_profile_page_contains_desription(self):
        response = Client().get('/profile')
        html_response = response.content.decode('utf8')
        self.assertNotIn('<h1 id="deskripsi"></h1>', html_response)


# class Lab6FunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         # chrome_options.add_argument('--dns-prefetch-disable')
#         # chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         # chrome_options.add_argument('disable-gpu')
#         service_log_path= './chromedriver.log'
#         service_args= ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#
#         super(Lab6FunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab6FunctionalTest, self).tearDown()
#
#     def test_input_todo(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         html_response = selenium.get('http://127.0.0.1:8000')
#         time.sleep(3)
#         # find the form element
#         status = selenium.find_element_by_name('status')
#         submit = selenium.find_element_by_name('submit')
#         # Fill the form with data
#         input = 'coba coba'
#         status.send_keys(input)
#         time.sleep(3)
#         # submitting the form
#         submit.send_keys(Keys.RETURN)
#         self.assertIn(input,selenium.page_source )
#
#     def test_Story_5_layout_1(self):
#         selenium = self.selenium
#         #  ------------- http://ppwstories.herokuapp.com/ ---------------------
#         home_url = 'http://ppwstories.herokuapp.com/'
#         selenium.get(home_url)
#
#         # Nama
#         element_nama  = selenium.find_element_by_class_name('nama')
#         nama = element_nama.text
#         self.assertEqual(nama,'Faishal Ammar')
#
#
#     def test_story_5_cek_ada_tombol_profile_dan_mengarah_ke_halaman_yang_di_ekspektasikan(self):
#         selenium = self.selenium
#         home_url = 'http://ppwstories.herokuapp.com/halamanBuku'
#         selenium.get(home_url)
#
#         tombol_kembali = selenium.find_element_by_class_name('btn')
#         tombol_kembali.click()
#         time.sleep(3)
#         #  ------------- http://ppwstories.herokuapp.com/ ---------------------
#         url_after_click_back_button = selenium.current_url
#
#         # Cek apakah url ketika di pencet tombol kembali, mengarah ke laman home awal
#         self.assertEqual(url_after_click_back_button,'http://ppwstories.herokuapp.com/')
#
#
#     def test_cek_css_background_color(self):
#         selenium = self.selenium
#         #  ------------- http://ppwstories.herokuapp.com/ ---------------------
#         home_url = 'http://ppwstories.herokuapp.com/'
#         selenium.get(home_url)
#
#         # Check CSS - Background Image
#         element_mainframe = selenium.find_element_by_class_name('mainFrame')
#         background_image = element_mainframe.value_of_css_property('background-image')
#         self.assertEqual(background_image, 'url("https://i.postimg.cc/FHBGSDTG/Webp.net-resizeimage.jpg")')
#
#         element_blue_layer = selenium.find_element_by_class_name('layer')
#         layer_color = element_blue_layer.value_of_css_property('background-color')
#         self.assertEqual(layer_color, 'rgba(56, 117, 215, 1)')
#
#     def test_Story_5_css_cek_warna_font(self):
#         selenium = self.selenium
#         #  ------------- http://ppwstories.herokuapp.com/ ---------------------
#         home_url = 'http://ppwstories.herokuapp.com/'
#         selenium.get(home_url)
#         # Nama dan deskripsi
#         nama  = selenium.find_element_by_class_name('nama')
#         warna_font_nama = nama.value_of_css_property('color')
#         self.assertEqual(warna_font_nama,'rgba(255, 255, 255, 1)')
#         deskripsi = selenium.find_element_by_class_name('capt')
#         warna_font_deskripsi = deskripsi.value_of_css_property('color')
#         self.assertEqual(warna_font_deskripsi,'rgba(255, 255, 255, 1)')
