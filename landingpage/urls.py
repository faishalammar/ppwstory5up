from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='home'),
    path('profile', views.profile, name='profile'),
    path('books', views.books, name='buku'),
    path('data', views.data, name='data'),
    #path('delete', views.delete, name='delete'),

]
