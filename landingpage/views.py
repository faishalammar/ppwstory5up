from .models import ApaKabar
from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render,redirect
from .forms import statusForm
import requests
import json

# Create your views here.



def index(request):
    data = ApaKabar.objects.all()
    form = statusForm()
    response = {'data': data, 'form': form}

    form = statusForm(request.POST)
    if request.method == "POST":
        form.save()
        return render(request, 'landingpage/landingpage.html',response)
    else:
        return render(request, 'landingpage/landingpage.html', response)

def profile(request):
    return render(request, 'landingpage/profile.html')

def books(request):
    return render(request, 'landingpage/Books.html')

def data(request):
    listBuku = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    jsonBook = listBuku.json()
    return JsonResponse(jsonBook)

# def delete(request):
#     data = ApaKabar.objects.all().delete()
#     response = {'data' : data}
#     return redirect('home',response)