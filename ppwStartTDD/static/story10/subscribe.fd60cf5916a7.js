$( function () {
    $('.alert').hide();
    $('.form-group').hide();

    $('.subscribe').click(function() {
        $('.subscriber').slideUp(500);
        $('.form-group').delay(400).show(200);
    });

    // tombol unsubscribe ------------------------------------------------


    $('body').on('click', '.unsubscribe', function () {
        var email = $(this).attr('id');
        var link = "delete/" + email+"";
        var tr_deleted = 'tr_' + email;
        document.getElementById(tr_deleted).remove();
        $.ajax({
            url:link,
        });
    });

    // Json data/subscriber ke table -------------------------------------

    $.ajax({
        url: '{% url "subscriber" %}',
        dataType : 'json',
        success:function(response){
            var s = "";
            console.log(response);
            var raw = response["Subscriber"];
            console.log(raw);
            for(i = 0; i < raw.length; i++){
                var index = '' + i;
                var result = raw[index];

                var nama = result['nama'];
                var email = result['email'];

                s+='<tr id=tr_'+ email + '>';
                s+='<td style="vertical-align: middle !important; text-align: center">' + (i+1) + '</td>';;
                s+='<td style="vertical-align: middle !important;">' + nama + '</td>';
                s+='<td style="vertical-align: middle !important;">' + email + '</td>';
                s+='<td <button id='+ email + ' class="unsubscribe btn btn-danger" name="Subscriber" style="margin-top: 3vh;margin-bottom: 5vh">unsubscribe</button>'
                s+='</tr>';
            }

            $('#list').html(s);
        }});

    // validasi email sama apa engga -----------------------------------

    $('#id_email').change(function () {
        email = $(this).val();
        console.log($(this).val());
        $.ajax({
            method: "GET",
            url: '{% url "validate" %}',
            data: {
                'email': email
            },
            dataType: 'json',
            success: function(data){
                console.log(data);
                console.log(data.test);
                if(data.is_taken){
                    $('.save').attr('disabled','disabled');
                    $('.alert-danger').slideDown();
                    $('#id_email').after($('.alert-danger'));
                }
                else {
                    $('.alert-danger').slideUp();
                    $('.save').attr('disabled',null);
                }
            }
        })
    });
});


$(document).ready(function () {
    $('.form-group').on('submit', function(){
        var name = $('#id_nama');
        var email = $('#id_email');
        var password = $('#id_password');
        var data = {
            'nama': name.val(),
            'email': email.val(),
            'password':password.val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
        };
        $.ajax({
            method: 'POST',
            url: '/story10/submit',
            type: 'json',
            data: data,
            success: function (result) {
                console.log('result : ' + result.success)
                $('.alert-success').show(300).delay(1000).hide(300);
                name.val('');
                email.val('');
                password.val('');

            }
        })
        event.preventDefault();
    })
})
