from django import forms
from .models import Registrasi
from django.db import models



class FormRegis(forms.ModelForm):

    class Meta:
        model = Registrasi
        fields = ('nama', 'email', 'password')
        widgets = {
            'nama': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Masukan Nama Anda'}),
            'email': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Masukan Email Anda', 'type': 'email'}),
            'password': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Masukan Password Anda', 'type': 'password'}),
        }