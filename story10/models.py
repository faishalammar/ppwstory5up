from django.db import models

# Create your models here

class Registrasi(models.Model):
    nama = models.CharField(max_length=40)
    email = models.CharField(max_length=40)
    password = models.CharField(max_length=15)

    def as_dict(self):
        return {
            "friend_name": self.nama,
            "email": self.email,
        }
