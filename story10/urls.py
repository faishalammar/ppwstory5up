from  django.urls import path
from . import views

urlpatterns = [
    path('',views.formRegistrasi,name='formRegistrasi'),
    path('submit', views.submitForm, name='submit'),
    path('ajax/validate',views.validate_email,name='validate'),
    path('delete/<email>', views.delete_user_by_email, name='delete'),
    path('data/subscriber', views.json_subscriber, name='subscriber')
]