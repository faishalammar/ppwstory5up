from django.shortcuts import render,reverse,redirect
from django.http import JsonResponse,HttpResponse
from .models import Registrasi
from .forms import FormRegis
import itertools
from django.views.decorators.csrf import csrf_exempt

import  json
# Create your views here.


def formRegistrasi(request):
    form = FormRegis()
    return render(request,'story10/post.html', {'form' : form})

def submitForm(request):
    data = {'success':False}
    form = FormRegis(request.POST)
    if request.method == "POST":
        if form.is_valid():
            print('abc')
            form.save()
            data['success'] = True
            data1 = json.dumps(data)

        else:
            print(form.errors)
            data1 = json.dumps(data)
        return HttpResponse(data1, content_type='application/json')
    else : return redirect('formRegistrasi')


def validate_email(request):
    email = request.GET.get('email',None)
    data = {
        'is_taken': Registrasi.objects.filter(email=email).exists(),
        "test":email
    }

    return JsonResponse(data)

def delete_user_by_email(request,email=None):
    Registrasi.objects.filter(email=str(email)).delete()
    return HttpResponse('OK')

def json_subscriber(request):
    data = Registrasi.objects.all()
    userDict = [{'nama' : i.nama,'email': i.email} for i in data]
    return HttpResponse(json.dumps({'Subscriber': userDict}), content_type='application/json')

# def formRegistrasi(request):
#     form = FormRegis(request.POST)
#     if request.method == "POST":
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse('formRegistrasi'))
#     else:
#         form = FormRegis()
#     return render(request,'story10/post.html', {'form' : form})
